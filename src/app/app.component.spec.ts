import { TestBed, async } from '@angular/core/testing';
import { CustomMaterialsModule } from './custom-materials/custom-materials.module';
import { LoginFormComponent } from './login-form/login-form.component';
import { RegistrationFormComponent } from './registration-form/registration-form.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { TableViewComponent } from './table-view/table-view.component';
import { CreateContractComponent } from './create-contract/create-contract.component';
import { FormsModule } from '@angular/forms';
import { AuthService } from './auth.service';
import { CdkTableModule } from '@angular/cdk';
import { AppComponent } from './app.component';
import { environment } from '../environments/environment';

// firebase
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        LoginFormComponent,
        RegistrationFormComponent,
        LandingPageComponent,
        TableViewComponent,
        CreateContractComponent
      ],
      imports: [
        CustomMaterialsModule,
        FormsModule,
        AngularFireModule.initializeApp(environment.firebaseConfig),
        AngularFireDatabaseModule,
        AngularFireAuthModule,
        CdkTableModule
      ],
      providers: [AuthService]
    }).compileComponents();
  }));

  it('should create the app', (() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it('Starts with no user', async(()=> {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.user).toEqual(null);
  }));

  it(`should have as title 'Contract Tracker'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('Contract Tracker');
  }));

  it('should render title in a md-toolbar tag', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('md-toolbar').textContent).toContain('Contract Tracker');
  }));

  it('should not show the logout button if there is no user', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled).not.toContain(compiled.querySelector('#logout-button'));
  }));

  it('should show logout, list view, grid view, and new contract buttons if a user is logged in', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    app.user = {'displayName': "Fake User", 'uid': 'XXX'};
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('#logout-button').textContent).toEqual('Logout');
    expect(compiled.querySelector('.view-modes').hasAttribute('hidden')).toEqual(false);
  }));

});

import { Component, OnInit } from '@angular/core';
import { AuthService } from './auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Contract Tracker';
  user = null;
  creatingContract:Boolean = false;
  finished:Boolean = false;
  constructor(
    private auth: AuthService
  ) {}

  ngOnInit() {
      this.auth.getAuthState().subscribe(
        (user) => {
          this.user = user
          this.finished = true;
        }
      );
  }

  newContract() {
    this.creatingContract = true;
  }

  logout() {
    this.auth.logout();
  }

}

import { Component, OnInit, Input, Inject } from '@angular/core';
import { AuthService } from '../auth.service';
import { MdDialog, MdDialogRef, MD_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent {

  registering: Boolean = false;
  error: String;
  email: String;

  constructor(
    private auth: AuthService,
    public dialog: MdDialog
  ) { }



  loginWithGoogle() {
    this.auth.loginWithGoogle().catch(error => {
      this.error = "Failed to authenticate with Google.";
    }
    );
  }

  login(loginForm) {
    let credentials = {
      email: loginForm.email,
      password: loginForm.password
    };

    this.auth.login(credentials).catch( error => {
      this.error = error.message;
    });

  }

  resetPassword() {
    let resetModal = this.dialog.open(ResetPasswordModal, {
      data: { email: this.email }
    });
    resetModal.afterClosed().subscribe(confirmation => {
      if (confirmation) {
        this.auth.resetPassword(confirmation).catch( error => {
          this.error = error.message;
        });
      }

    });
  }

  goToRegistration() {
    this.registering = true;
  }

}

@Component({
  selector: 'reset-password-modal',
  templateUrl: './reset-password-dialog.component.html',
})
export class ResetPasswordModal {
  constructor(
    public dialogRef: MdDialogRef<ResetPasswordModal>,
    @Inject(MD_DIALOG_DATA) public data: any
  ) {}
}

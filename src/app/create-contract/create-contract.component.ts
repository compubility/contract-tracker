import { Component, Input, Output, EventEmitter, OnInit, ViewChild } from '@angular/core';
import { Position } from '../position';
import { MdDialog, MdDialogRef } from '@angular/material';
import { ContractService } from '../contract.service';

@Component({
  selector: 'create-contract',
  templateUrl: './create-contract.component.html',
  styleUrls: ['./create-contract.component.css']
})
export class CreateContractComponent implements OnInit {

  @Output() close = new EventEmitter();
  @Input() contractID: String;
  @ViewChild('contractForm') contractForm;

  editingContract: Boolean = false;
  positions: Array<Position> = [new Position()];
  public creatingContract: Boolean = true;
  loadedContract: any;
  loading: Boolean = true;
  error: String;

  constructor(
    public dialog: MdDialog,
    private CS: ContractService
  ) {}

  addPosition() {
    this.positions.push(new Position());
  }

  removePosition(idx) {
    this.positions.splice(idx, 1);
  }

  confirmCancel() {
    let confirmCancel = this.dialog.open(ConfirmationDialog);
    confirmCancel.afterClosed().subscribe(confirmation => {
      if (confirmation === "true") {
        this.close.emit(null);
      }
    });
  }

  createPositions() {
    var position;
    for (let i = 0; i < this.positions.length; i++) {
      position = this.positions[i];
      this.positions[i] = new Position(
        position.title,
        Number(position.hours),
        Number(position.rate),
        position.startDate,
        position.endDate
      );
    }
  }

  createContract(contractForm) {
    this.checkIncompletePositions();
    if (!this.error) {
      this.createPositions();
      contractForm.positions = this.positions;
      this.CS.createContract(contractForm);
      this.close.emit(null);
    }
  }

  saveContract(contractForm) {
    this.checkIncompletePositions();
    if (!this.error) {
      this.createPositions();
      contractForm.positions = this.positions;
      contractForm.id = this.loadedContract.$key;
      this.CS.saveContract(contractForm);
      this.close.emit(null);
    }

  }

  startAfterEnd(start, end) {
    if (!start) {
      this.error = "Please specify a start date.";
    } else if (!end) {
      this.error = "Please specify an end date.";
    } else if (end <= start) {
      this.error = "Position start date cannot be after end date.";
    }
  }

  checkIncompletePositions() {
    // Validate position entries
    this.error = undefined;
    if (!this.positions.length) {
      this.error = "At least one position must be created.";
      return;
    }
    for (let i = 0; i < this.positions.length; i++) {
      let current = this.positions[i];
      if (!(current.title && current.hours && current.rate)) {
        this.error = "Please fill out all fields."
        return;
      }
      this.startAfterEnd(current.startDate, current.endDate)
      if (this.error) {
        return;
      }
    }

  }

  ngOnInit() {
    // if we have a contract ID, let's load it up so it can be edited
    if (this.contractID) {
      let contract = this.CS.getContract(this.contractID).subscribe(result => {
        this.loadedContract = result;
        this.positions = result.positions;
        this.editingContract = true;
        this.loading = false;
      });
    } else {
      this.loading = false;
    }
  }

  ngAfterViewInit() {
  }
}

@Component({
  selector: 'confirmation-dialog',
  templateUrl: './confirmation-dialog.component.html',
})
export class ConfirmationDialog {
  constructor(public dialogRef: MdDialogRef<ConfirmationDialog>) {}
}

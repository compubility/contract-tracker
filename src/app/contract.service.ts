import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';

import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import * as firebase from 'firebase/app';

@Injectable()
export class ContractService {

  public userEntry: FirebaseObjectObservable<any>;
  public userContracts: FirebaseListObservable<any[]>;

  constructor(
    private auth: AuthService,
    public db: AngularFireDatabase
  ) {
    this.userEntry = this.auth.getUserEntry();
  }


  getContracts() {
    return this.db.list('/contracts');
  }

  getContract(contractID) {
    return this.db.object(`/contracts/${contractID}`);
  }

  createContract(details) {
    details.creator = this.auth.getCurrentUser().uid;
    this.db.list('/contracts').push(details);
  }

  saveContract(details) {
    let key = details.id;
    delete details.id;
    this.db.object(`/contracts/${key}`).update(details);

  }

  closeContract(contractID) {
   // for now fully remove </3
   this.db.object(`/contracts/${contractID}`).remove();
   /*
    this.db.object(`/contracts/${contractID}`).update({
      closed: true
    });
    */
  }


}

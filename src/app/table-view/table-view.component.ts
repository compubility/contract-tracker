import { Component, OnInit, ViewChild } from '@angular/core';
import { DataSource } from '@angular/cdk';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';

import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';

import { CdkTableModule } from '@angular/cdk';
import { ContractService } from '../contract.service';

@Component({
  selector: 'table-view',
  templateUrl: './table-view.component.html',
  styleUrls: ['./table-view.component.css']
})
export class TableViewComponent implements OnInit {

  editingContract: Boolean = false;
  tableHeaders = ['contract', 'client', 'positionsAvailable', 'actions'];
  dataSource: MyDataSource;
  contracts: FirebaseListObservable<any[]>;
  user = null;
  contractID: String;

  constructor(
    public CS: ContractService
  ) {
  }

  editContract(contractID) {
    this.contractID = contractID;
    this.editingContract = true;
  }

  closeContract(contractID) {
    this.CS.closeContract(contractID);
  }

  ngOnInit() {
    this.contracts = this.CS.getContracts();
    this.dataSource = new MyDataSource(this.contracts);
  }

}

export class MyDataSource extends DataSource<any> {
  constructor(private data: FirebaseListObservable<any[]>) {
    super();
  }
   /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): FirebaseListObservable<any[]> {
    return this.data;
  }

  disconnect() {}

  }

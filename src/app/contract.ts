import { Position } from './position';

export class Contract {
  contract: String;
  client: String;
  clientId: Number;
  positions: Array<Position>;
}

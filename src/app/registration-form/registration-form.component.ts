import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { EmailValidator } from '@angular/forms';
import { AuthService } from '../auth.service';


@Component({
  selector: 'registration-form',
  templateUrl: './registration-form.component.html',
  styleUrls: ['./registration-form.component.css']
})
export class RegistrationFormComponent implements OnInit {

  @Output() close = new EventEmitter();
  error:String;

  constructor(
    private auth: AuthService
  ) { }

  goToLogin() {
    this.close.emit(null);
  }

  registerWithGoogle() {
    this.auth.loginWithGoogle();
  }

  registerWithEmail(registrationForm) {
    let credentials = {
      email: registrationForm.email,
      password: registrationForm.password
    };
    this.auth.register(credentials).catch( error => {
      this.error = error.message;
    });
  }

  ngOnInit() {
  }

}

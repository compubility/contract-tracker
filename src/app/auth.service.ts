import { Injectable } from '@angular/core';

import { AngularFireDatabase, FirebaseObjectObservable } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs/Observable';
import * as firebase from 'firebase/app';

@Injectable()
export class AuthService {
  public authState: Observable<firebase.User>;
  public currentUser: firebase.User = null;
  public userEntry: FirebaseObjectObservable<any>;

  constructor(
    private afAuth: AngularFireAuth,
    private db: AngularFireDatabase
  ) {
    this.authState = this.afAuth.authState;
    this.authState.subscribe((user) => {
      if (user) {
        this.currentUser = user;
        this.userEntry = this.db.object(`/users/${user.uid}`);
      } else {
        this.currentUser = null;
      }
    });
  }

  getCurrentUser() {
    return this.currentUser;
  }

  getUserEntry() {
    return this.userEntry;
  }

  getAuthState() {
    return this.authState;
  }

  loginWithGoogle() {
    return this.afAuth.auth.signInWithPopup(
      new firebase.auth.GoogleAuthProvider()
    );
  }

  login(credentials) {
    return this.afAuth.auth.signInWithEmailAndPassword(credentials.email, credentials.password);
  }

  register(credentials) {
    return this.afAuth.auth.createUserWithEmailAndPassword(credentials.email, credentials.password);
  }

  resetPassword(email) {
    return this.afAuth.auth.sendPasswordResetEmail(email);
  }

  logout() {
    return this.afAuth.auth.signOut();
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TableViewComponent } from '../table-view/table-view.component';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CustomMaterialsModule } from '../custom-materials/custom-materials.module';
import { CdkTableModule } from '@angular/cdk';
import { AuthService } from '../auth.service';
import { CreateContractComponent } from '../create-contract/create-contract.component';
import { ContractService } from '../contract.service';
import { environment } from '../../environments/environment';

// firebase
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';


import { LandingPageComponent } from './landing-page.component';

describe('LandingPageComponent', () => {
  let component: LandingPageComponent;
  let fixture: ComponentFixture<LandingPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        LandingPageComponent,
        TableViewComponent,
        CreateContractComponent
      ],
      imports: [
        BrowserAnimationsModule,
        CustomMaterialsModule,
        FormsModule,
        CdkTableModule,
        AngularFireModule.initializeApp(environment.firebaseConfig),
        AngularFireDatabaseModule,
        AngularFireAuthModule
      ],
      providers: [
        AuthService,
        ContractService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LandingPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, Input, OnInit } from '@angular/core';
import { TableViewComponent } from '../table-view/table-view.component';
import { ContractService } from '../contract.service';

@Component({
  selector: 'landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.css']
})
export class LandingPageComponent implements OnInit {

  @Input() contractID:String;

  contracts = [];
  finished: Boolean = false;
  contractsSubscription = null;

  constructor(
    public CS: ContractService
  ) {
    this.contractsSubscription = this.CS.getContracts().subscribe(contracts=>{
      if(contracts){
        this.contracts = contracts;
      } else {
        this.contracts = [];
      }
      });

  }

  ngOnInit() {
    this.finished = true;
  }

  ngOnDestroy() {
    this.contractsSubscription.unsubscribe();
  }

}

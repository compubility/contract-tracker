// Modules
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from "@angular/flex-layout";
import { CustomMaterialsModule } from './custom-materials/custom-materials.module';
import { CdkTableModule } from '@angular/cdk';
import { MdDialog, MdDialogRef } from '@angular/material';

// firebase
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';

// swipe gestures if I decide to do anything crazy on mobile
import 'hammerjs';

// Components
import { AppComponent } from './app.component';
import { LoginFormComponent, ResetPasswordModal } from './login-form/login-form.component';
import { RegistrationFormComponent } from './registration-form/registration-form.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { TableViewComponent } from './table-view/table-view.component';
import { GridViewComponent } from './grid-view/grid-view.component';
import { CreateContractComponent, ConfirmationDialog } from './create-contract/create-contract.component';

// Services
import { AuthService } from './auth.service';
import { ContractService } from './contract.service';

import { environment } from '../environments/environment';


@NgModule({
  declarations: [
    AppComponent,
    LoginFormComponent,
    RegistrationFormComponent,
    LandingPageComponent,
    TableViewComponent,
    GridViewComponent,
    CreateContractComponent,
    ConfirmationDialog,
    ResetPasswordModal
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    FlexLayoutModule,
    CdkTableModule,
    CustomMaterialsModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule
  ],
  entryComponents: [
    ConfirmationDialog,
    ResetPasswordModal
  ],
  providers: [
    AuthService,
    ContractService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

export class Position {
  title: String;
  hours: Number;
  rate: Number;
  startDate: String;
  endDate: String;
  constructor(Title?:String, Hours?:Number, Rate?:Number, Start?:String, End?:String){
    this.title = Title;
    this.hours = Hours;
    this.rate = Rate;
    this.startDate = Start;
    this.endDate = End;

  }
}

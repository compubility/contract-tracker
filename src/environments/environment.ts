// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyDxgf6FGkys2WY88Bt5toS8YeOJ26yBDks",
    authDomain: "contract-tracker-a5c92.firebaseapp.com",
    databaseURL: "https://contract-tracker-a5c92.firebaseio.com",
    projectId: "contract-tracker-a5c92",
    storageBucket: "contract-tracker-a5c92.appspot.com",
    messagingSenderId: "962733810298"
  }
};
